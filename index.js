const txtFirstName = document.querySelector('#txt-first-name');

const txtLastName = document.querySelector('#txt-last-name');

const spanFullName = document.querySelector('#span-full-name');

const spanGreetings = document.querySelector('.span-greetings');

// Alternatives for document.querySelector();
/*
	document.getElementById('txt-firstName');
	document.getElementByClassName('txt-inputs');
	document.getElementByTagName('inputs');
*/


txtFirstName.addEventListener('keyup', (event)=>{
	spanFullName.innerHTML = txtFirstName.value;
})


txtLastName.addEventListener('keyup', (event) => {
  spanFullName.innerHTML = txtLastName.value;
});

txtFirstName.addEventListener('keyup', (event)=>{
	console.log(event.target);
	console.log(event.target.value);
})


txtLastName.addEventListener('keyup', (event)=>{
	console.log(event.target);
	console.log(event.target.value);
})
// Additional Example & Mini-activity

const greeting = () => {
	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;
	let greetings = 'Hi, welcome! ';
	spanGreetings.innerHTML = greetings + firstName + ' ' + lastName;

}

txtFirstName.addEventListener('keyup', greeting);
txtLastName.addEventListener('keyup', greeting);


const fullName = () => {
  let firstName = txtFirstName.value;
  let lastName = txtLastName.value;
  spanFullName.innerHTML = firstName + ' ' + lastName;
};

txtFirstName.addEventListener('keyup', fullName);
txtLastName.addEventListener('keyup', fullName);
